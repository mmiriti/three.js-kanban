## TODO

- [x] Drag and drop (change column)
- [x] Edit text while adding
- [x] Board
  - [x] Init board with number of columns
- [ ] Cards
    - [x] Word wrapping in notes
    - [x] Center text
    - [x] Edit card
    - [x] Remove card (by moving it outside of the board)
    - [ ] Auto enlarge note
- [ ] Columns
    - [x] Edit column title
    - [x] Edit column title when added
    - [x] Move column left-right
    - [ ] Remove column
    - [x] bug: adding more than 8 cards
        - [x] auto-increase board height

- [x] Zoom in/out
- [x] Panning

## Won't do

- ~~~[ ] Edit form clicks are propagating~~~
- ~~~[ ] fix: closing viewd note~~~