import { Tween } from "@tweenjs/tween.js";
import { BoxGeometry, Mesh, MeshBasicMaterial, Object3D, Vector3 } from "three";
import Column from "./Column";
import Note from "./Note";

export default class Board extends Object3D {
  columns: Column[] = [];
  boardWidth: number = 0;
  boardHeight: number = 5;

  private _mesh: Mesh;

  private _adder: Mesh;
  private _draggingNote: Note = null;
  private _draggingColumn: Column = null;

  private _dragPos: Vector3 = new Vector3();
  private _currentDragCol: number;
  private _bgMesh: any;

  constructor() {
    super();

    this._bgMesh = new Mesh(
      new BoxGeometry(1, 1, 1),
      new MeshBasicMaterial({ color: 0x888888 })
    );
    this._bgMesh.position.z = -0.1;
    this._bgMesh.name = "board_bg";
    this.add(this._bgMesh);

    this._mesh = new Mesh(
      new BoxGeometry(1, 1, 1),
      new MeshBasicMaterial({ color: 0xffffff })
    );
    this._mesh.name = "board";
    this._mesh.position.z = -0.025;
    this.add(this._mesh);

    this._adder = new Mesh(
      new BoxGeometry(1, 0.3, 0.01),
      new MeshBasicMaterial({ color: 0xcccccc })
    );
    this._adder.name = "column_adder";
    this.add(this._adder);

    this._adder.userData["down"] = () => {
      const newColumn = this.addColumn("");

      setTimeout(() => {
        newColumn.editTitle();
      });
    };
  }

  orderColumns(anim: boolean = false) {
    this.boardWidth = (this.columns.length + 1) * 1.1 + 1;

    for (const column of this.columns)
      if (column.columnHeight > this.boardHeight)
        this.boardHeight = column.columnHeight;

    this._mesh.scale.set(this.boardWidth, this.boardHeight, 0.05);
    this._bgMesh.scale.set(this.boardWidth + 15, this.boardHeight + 4, 0.1);

    for (let n = 0; n < this.columns.length; n++) {
      const column = this.columns[n];

      if (column != this._draggingColumn) {
        const tx = 1 + -this.boardWidth / 2 + n * 1.1;
        const ty = this.boardHeight / 2;

        if (anim) {
          new Tween(column.position)
            .to({ x: tx, y: ty, z: 0 }, 200)
            .onUpdate((props: any) => {
              column.setPos(props.x, props.y);
              column.position.z = props.z;
            })
            .start();
        } else {
          column.setPos(tx, ty);
        }
      }
    }

    const atx = 1 + -this.boardWidth / 2 + this.columns.length * 1.1;
    const aty = this.boardHeight / 2 - 0.25;

    if (anim) {
      new Tween(this._adder.position).to({ x: atx, y: aty }).start();
    } else {
      this._adder.position.x = atx;
      this._adder.position.y = aty;
    }
  }

  mouseMove(point: Vector3, outside: boolean) {
    this._dragPos.copy(point);

    if (this._draggingColumn) {
      this._draggingColumn.setPos(
        this._dragPos.x,
        this._draggingColumn.position.y
      );

      const prevIndex = this.columns.indexOf(this._draggingColumn);
      this.columns.sort((a, b) => (a.position.x < b.position.x ? -1 : 1));
      const postIndex = this.columns.indexOf(this._draggingColumn);

      if (postIndex != prevIndex) this.orderColumns(true);
    }

    if (this._draggingNote) {
      this._draggingNote.toDelete = outside;
      this._draggingNote.position.x = point.x;
      this._draggingNote.position.y = point.y;

      for (let colIndex = 0; colIndex < this.columns.length; colIndex++) {
        const col = this.columns[colIndex];

        if (
          point.x > col.position.x - col.columnWidth / 2 &&
          point.x < col.position.x + col.columnWidth / 2
        ) {
          const newIndex = col.posIndex(point.y);

          if (
            this._draggingNote.column != col ||
            this._draggingNote.index != newIndex
          ) {
            if (this._draggingNote.column != null)
              this._draggingNote.column.removeNote(this._draggingNote);
            col.addNote(this._draggingNote, newIndex, true, true);
          }

          if (this._currentDragCol != colIndex) {
            this._currentDragCol = colIndex;

            for (let c = 0; c < this.columns.length; c++) {
              if (c != colIndex) this.columns[c].orderNotes(true);
            }
          }
        }
      }
    }
  }

  mouseUp() {
    if (this._draggingColumn) {
      this.releaseColumn(this._draggingColumn);
    }
  }

  startDragColumn(column: Column) {
    this._draggingColumn = column;

    new Tween(column.position)
      .to({ x: this._dragPos.x, z: column.position.z + 0.1 }, 100)
      .onUpdate((props) => {
        this._draggingColumn.setPos(props.x, props.y);
      })
      .start();

    this.orderColumns(true);
  }

  releaseColumn(column: Column) {
    this._draggingColumn = null;
    this.orderColumns(true);
  }

  startDragNote(note: Note) {
    this._draggingNote = note;
    this._currentDragCol = this.columns.indexOf(this._draggingNote.column);
    new Tween(this._draggingNote.position)
      .to(
        { x: this._dragPos.x, y: this._dragPos.y, z: note.position.z + 0.1 },
        100
      )
      .start();

    this.add(this._draggingNote);
  }

  releaseNote(note: Note) {
    this._draggingNote = null;
  }

  addColumn(column: string | Column) {
    const columnToAdd =
      column instanceof Column ? column : new Column(1, column);

    this.columns.push(columnToAdd);

    this.add(columnToAdd);

    columnToAdd.board = this;

    this.orderColumns(false);

    return columnToAdd;
  }
}
