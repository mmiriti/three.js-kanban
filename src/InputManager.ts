import TextTexture from "./TextTexture";

type InputCallback = (text: string) => void;

class InputManager {
  input: HTMLInputElement;

  target: { texture: TextTexture };

  private _blinkTimer: NodeJS.Timer;
  private _text: string = "";

  private _callback: InputCallback;

  constructor() {
    this.input = document.getElementById("hiddenInput") as HTMLInputElement;

    this.input.addEventListener("input", (event: InputEvent) => {
      if (this.target != null) {
        this._text = this.input.value;

        this._updateTextureText();
      }
    });

    this.input.addEventListener("keydown", (event) => {
      if (event.key == "Enter" || event.key == "Escape") this.endInput();
    });
  }

  private _updateTextureText() {
    this.target.texture.text = this._text;
  }

  endInput() {
    clearInterval(this._blinkTimer);
    this.input.blur();
    if (this.target) {
      this.target.texture.showCursor = false;
      this._updateTextureText();
    }
    this.target = null;
    this._callback(this._text);
  }

  startInput(initial: string, texture: TextTexture, cb: InputCallback) {
    if (this.target != null) this.endInput();

    this.input.value = initial;
    this.input.focus();

    this.target = {
      texture,
    };

    this._blinkTimer = setInterval(() => {
      this.target.texture.showCursor = !this.target.texture.showCursor;
    }, 500);

    texture.showCursor = true;
    this._text = initial;
    this._callback = cb;

    this._updateTextureText();
  }
}

const inputManager = new InputManager();

export default inputManager;
