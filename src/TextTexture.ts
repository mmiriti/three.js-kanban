import { CanvasTexture } from "three";

export default class TextTexture {
  texture: CanvasTexture;
  canvas: HTMLCanvasElement;

  height: number;
  width: number;

  centered: boolean = false;
  fontSize: number = 50;

  textureWidth: number;
  textureHeight: number;
  padding: {
    left: number;
    right: number;
    top: number;
    bottom: number;
  };
  fillStyle: string;
  textStyle: string;

  private _text: string;

  get text(): string {
    return this._text;
  }

  set text(val: string) {
    this._text = val;

    this.updateText();
  }

  private _showCursor: boolean = false;

  get showCursor(): boolean {
    return this._showCursor;
  }

  set showCursor(val: boolean) {
    this._showCursor = val;

    this.updateText();
  }

  private _ctx: CanvasRenderingContext2D;

  private updateText() {
    ((c) => {
      c.fillStyle = this.fillStyle;
      c.fillRect(0, 0, this.width, this.height);
      c.fillStyle = this.textStyle;
      c.font = this.fontSize.toString() + "px sans";
      c.textBaseline = "hanging";

      const words = this._text.split(" ");

      let line = "";

      let tx = this.padding.left;
      let ty = this.padding.top;

      let w = 0;

      const maxWidth =
        this.textureWidth - (this.padding.left + this.padding.right);

      const drawLine = (line: string) => {
        const lmetrics = c.measureText(line);

        const ltx = this.centered ? tx + (maxWidth - lmetrics.width) / 2 : tx;
        c.fillText(line, ltx, ty);

        w = ltx + lmetrics.width;
      };

      for (let n = 0; n < words.length; n++) {
        const testLine = line + words[n] + " ";
        const metrics = c.measureText(testLine);
        let testWidth = metrics.width;
        if (testWidth > maxWidth && n > 0) {
          drawLine(line);
          line = words[n] + " ";
          ty += 60;
        } else {
          line = testLine;
        }
        drawLine(line);
      }

      if (this._showCursor) {
        c.fillText("|", w - 20, ty);
      }
    })(this._ctx);

    this.texture.needsUpdate = true;
  }

  constructor(
    width: number,
    height: number,
    fillStyle: string,
    textStyle: string,
    text: string,
    centered: boolean = false,
    fontSize: number = 50,
    padding: { left: number; right: number; top: number; bottom: number } = {
      left: 50,
      right: 50,
      top: 50,
      bottom: 50,
    }
  ) {
    this.fillStyle = fillStyle;
    this.textStyle = textStyle;
    this.fontSize = fontSize;

    this.canvas = document.createElement("canvas") as HTMLCanvasElement;
    this.canvas.width = this.width = this.textureWidth = width;
    this.canvas.height = this.height = this.textureHeight = height;

    this.padding = padding;

    this._ctx = this.canvas.getContext("2d");

    this.texture = new CanvasTexture(this.canvas);
    this.centered = centered;
    this.text = text;
  }
}
