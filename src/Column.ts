import * as TWEEN from "@tweenjs/tween.js";
import { BoxGeometry, Mesh, MeshBasicMaterial, Object3D } from "three";
import Board from "./Board";
import inputManager from "./InputManager";
import Note from "./Note";
import TextTexture from "./TextTexture";

const NOTE_GAP = 0.05;
const TITLE_GAP = 0.45;

export default class Column extends Object3D {
  notes: Note[] = [];
  columnWidth: number;
  columnHeight: number = 0;

  board: Board;

  private _title: string;
  private _titleDownTime: number;
  private _startDragTimer: NodeJS.Timeout;

  set title(val: string) {
    this._title = val;
    this._titleTexture.text = val;
  }

  get title(): string {
    return this._title;
  }

  private _adder: Mesh;

  private _titleTexture: TextTexture;
  private _titleMesh: Mesh<BoxGeometry, MeshBasicMaterial>;

  constructor(columnWidth: number = 1, title: string) {
    super();

    this.columnWidth = columnWidth;

    this._adder = new Mesh(
      new BoxGeometry(1, 0.3, 0.01),
      new MeshBasicMaterial({
        color: 0xcccccc,
      })
    );

    this._titleTexture = new TextTexture(
      512,
      512 * 0.3,
      "#8888ff",
      "#ffffff",
      title,
      true,
      65
    );

    this._titleMesh = new Mesh(
      new BoxGeometry(1, 0.3, 0.01),
      new MeshBasicMaterial({ map: this._titleTexture.texture })
    );

    this._title = title;

    this._titleMesh.position.y = -0.25;

    this._titleMesh.userData["down"] = () => {
      this._titleDownTime = Date.now();

      this._startDragTimer = setTimeout(() => {
        this._startDragTimer = null;

        this.board.startDragColumn(this);
      }, 350);
    };

    this._titleMesh.userData["up"] = this._titleMesh.userData["up_out"] =
      () => {
        if (this._startDragTimer != null) clearTimeout(this._startDragTimer);

        if (Date.now() - this._titleDownTime < 350) {
          setTimeout(() => {
            this.editTitle();
          });
        }
      };

    this.add(this._titleMesh);

    this._adder.name = "note_adder";

    this._adder.userData["down"] = () => {
      const newNote = this.addNote("");
      setTimeout(() => {
        newNote.startEdit();
      });
    };

    this.add(this._adder);

    this.orderNotes(false);
  }

  editTitle() {
    inputManager.startInput(
      this.title,
      this._titleTexture,
      (text) => (this.title = text)
    );
  }

  startDrag(note: Note) {
    this.board.startDragNote(note);
  }

  removeNote(note: Note) {
    if (note.column == this) {
      this.notes.splice(this.notes.indexOf(note), 1);
      note.column = null;
    }
  }

  addNote(
    note: Note | string,
    at?: number,
    anim: boolean = false,
    skipBoard = false
  ) {
    const noteToAdd = note instanceof Note ? note : new Note(note);

    noteToAdd.column = this;

    this.board.add(noteToAdd);

    if (at == null) at = this.notes.length;

    this.notes.splice(at, 0, noteToAdd);
    noteToAdd.index = at;

    this.orderNotes(anim, skipBoard);

    return noteToAdd;
  }

  posIndex(posY: number) {
    let ny = this.position.y - TITLE_GAP;

    for (let index = 0; index < this.notes.length; index++) {
      const note = this.notes[index];

      if (posY > ny - note.noteHeight) return index;

      ny -= note.noteHeight + NOTE_GAP;
    }

    return this.notes.length;
  }

  setPos(xpos: number, ypos: number) {
    this.position.x = xpos;
    this.position.y = ypos;

    this.orderNotes(false, true);
  }

  orderNotes(anim: boolean = false, skipBoard = false) {
    let ny = this.position.y - TITLE_GAP;

    let totalNotesHeight = 0;

    for (const note of this.notes) {
      if (!note.dragging) {
        const tx = this.position.x;
        const ty = ny - note.noteHeight / 2;
        const tz = this.position.z;

        if (anim) {
          new TWEEN.Tween(note.position)
            .to({ x: tx, y: ty, z: tz }, 200)
            .start();
        } else {
          note.position.x = tx;
          note.position.y = ty;
          note.position.z = tz;
        }
      }

      totalNotesHeight += note.noteHeight + NOTE_GAP;

      ny -= note.noteHeight + NOTE_GAP;
    }

    const addPos = ny - this.position.y - 0.2;

    this.columnHeight = totalNotesHeight + 0.6 + TITLE_GAP * 2;

    if (this.board && !skipBoard) this.board.orderColumns(false);

    if (anim) {
      new TWEEN.Tween(this._adder.position).to({ y: addPos }, 200).start();
    } else {
      this._adder.position.y = addPos;
    }
  }
}
