import * as TWEEN from "@tweenjs/tween.js";
import {
  Object3D,
  PerspectiveCamera,
  Raycaster,
  Scene,
  Vector2,
  WebGLRenderer,
} from "three";
import Board from "./Board";

async function main() {
  const renderer = new WebGLRenderer({
    antialias: true,
  });
  document.body.appendChild(renderer.domElement);

  const camera = new PerspectiveCamera(
    50,
    window.innerWidth / window.innerHeight,
    0.1,
    1000
  );

  camera.position.set(0, 0, 6);

  camera.lookAt(0, 0, 0);

  let panning: Vector2 = null;

  const scene = new Scene();

  const board = new Board();
  board.addColumn("ToDo");
  board.addColumn("Doing");
  board.addColumn("Done");
  scene.add(board);

  const pointer = new Vector2();
  const raycaster = new Raycaster();

  window.addEventListener("pointermove", (event) => {
    if (panning == null) {
      pointer.set(
        (event.clientX / window.innerWidth) * 2 - 1,
        -(event.clientY / window.innerHeight) * 2 + 1
      );

      raycaster.setFromCamera(pointer, camera);
      const objs = raycaster.intersectObjects(scene.children);

      for (const obj of objs) {
        if (["board", "board_bg"].indexOf(obj.object.name) != -1) {
          board.mouseMove(obj.point, obj.object.name == "board_bg");
          break;
        }
      }
    } else {
      const dx = event.clientX - panning.x;
      const dy = event.clientY - panning.y;

      camera.position.x -= dx / 100;
      camera.position.y += dy / 100;

      panning.set(event.clientX, event.clientY);
    }
  });

  const pressedObjs: Object3D[] = [];

  window.addEventListener("pointerdown", (event) => {
    if (event.button == 0) {
      raycaster.setFromCamera(pointer, camera);

      const objs = raycaster.intersectObjects(scene.children);

      for (const obj of objs) {
        if ("down" in obj.object.userData) {
          obj.object.userData["down"]();
          pressedObjs.push(obj.object);
        }
      }
    } else if (event.button == 2) {
      event.preventDefault();
      event.stopImmediatePropagation();
      event.stopPropagation();

      panning = new Vector2(event.clientX, event.clientY);
    }
  });

  window.addEventListener("pointerup", (event) => {
    if (event.button == 0) {
      raycaster.setFromCamera(pointer, camera);

      const objs = raycaster.intersectObjects(scene.children);

      for (const obj of objs) {
        if ("down" in obj.object.userData) {
          const index = pressedObjs.indexOf(obj.object);

          if (index != -1) {
            pressedObjs.splice(index, 1);

            if ("up" in obj.object.userData) obj.object.userData["up"]();
          }
        }
      }

      for (const obj of pressedObjs) {
        if ("up_out" in obj.userData) {
          obj.userData["up_out"]();
        }
      }

      pressedObjs.splice(0, pressedObjs.length);

      board.mouseUp();
    } else if (event.button == 2) {
      panning = null;
    }
  });

  window.addEventListener("contextmenu", (event) => {
    event.preventDefault();
  });

  window.addEventListener("wheel", (event) => {
    camera.position.z += event.deltaY / 100;

    if (camera.position.z < 2) camera.position.z = 2;
    if (camera.position.z > 9) camera.position.z = 9;
  });

  function resize() {
    renderer.setSize(window.innerWidth, window.innerHeight);
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
  }

  resize();

  window.addEventListener("resize", (event) => {
    resize();
  });

  function animate(time: number) {
    TWEEN.update(time);
    renderer.render(scene, camera);
    requestAnimationFrame(animate);
  }

  requestAnimationFrame(animate);
}

main();
