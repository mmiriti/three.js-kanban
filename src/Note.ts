import { Tween } from "@tweenjs/tween.js";
import { BoxGeometry, Mesh, MeshBasicMaterial, Object3D, Vector3 } from "three";
import Column from "./Column";
import inputManager from "./InputManager";
import TextTexture from "./TextTexture";

export default class Note extends Object3D {
  column: Column;
  index: number;
  noteWidth: number;
  noteHeight: number;
  dragging: boolean = false;
  toDelete: boolean = false;

  private _origPos: Vector3 = new Vector3();
  private _mesh: Mesh;
  private _textTexture: TextTexture;
  private _startDragTimer: NodeJS.Timeout;
  private _downTime: number;
  private _editing: boolean = false;
  private _text: string = "";

  get text() {
    return this._text;
  }

  set text(val: string) {
    this._text = val;
    this._textTexture.text = val;
  }

  constructor(
    text: string = "",
    noteWidth: number = 1,
    noteHeight: number = 0.8,
    fillStyle: string = "#ffff99"
  ) {
    super();

    this.noteWidth = noteWidth;
    this.noteHeight = noteHeight;

    this._textTexture = new TextTexture(
      noteWidth * 512,
      noteHeight * 512,
      fillStyle,
      "#000000",
      text
    );

    this._mesh = new Mesh(
      new BoxGeometry(noteWidth, noteHeight, 0.01),
      new MeshBasicMaterial({
        map: this._textTexture.texture,
      })
    );
    this._mesh.name = "note_paper";
    this._mesh.position.z = 0.005;
    this._mesh.userData["down"] = () => {
      this._downTime = Date.now();
      this._startDragTimer = setTimeout(() => {
        this._startDragTimer = null;
        this._origPos.copy(this.position);
        this.dragging = true;
        this.endEdit();
        this.column.startDrag(this);
      }, 350);
    };

    this._mesh.userData["up"] = this._mesh.userData["up_out"] = () => {
      if (this._startDragTimer != null) clearTimeout(this._startDragTimer);

      if (this.dragging) {
        this.column.board.releaseNote(this);
        this.dragging = false;

        const board = this.column.board;

        if (this.toDelete) {
          new Tween(this.scale)
            .to({ x: 0, y: 0, z: 0 }, 250)
            .start()
            .onComplete(() => {
              board.remove(this);
            });
          this.column.removeNote(this);
        }

        for (const col of board.columns) col.orderNotes(true);
      } else {
        if (Date.now() - this._downTime < 350) {
          if (this._editing) this.endEdit();
          else this.startEdit();
        }
      }
    };

    this.add(this._mesh);
  }

  startEdit() {
    this._editing = true;

    inputManager.startInput(this.text, this._textTexture, (text) => {
      this.text = text;
    });
  }

  endEdit() {
    if (this._editing) {
      this._editing = false;

      inputManager.endInput();
    }
  }
}
